/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/components/**/*.{js,vue,ts}",
    "./src/views/**/*.vue",
    "./src/App.vue",
    "./src/error.vue",
  ],
  theme: {
    extend: {},
    fontFamily: {
      'co-text-light': ['CoTextLight', 'sans-serif'],
      'co-text': ['CoText', 'sans-serif'],
      'co-text-bold': ['CoTextBold', 'sans-serif'],
    },
    boxShadow: {
      sm: '0 1px 2px 0 rgb(0 0 0 / 0.05)',
      DEFAULT: '0 0 0 0.075vw #fff, 0 0 0.7vw 0.7vw hsla(0,0%,100%,.25)',
      md: '0 4px 6px -1px rgb(0 0 0 / 0.1), 0 2px 4px -2px rgb(0 0 0 / 0.1)',
      lg: '0 10px 15px -3px rgb(0 0 0 / 0.1), 0 4px 6px -4px rgb(0 0 0 / 0.1)',
      xl: '0 20px 25px -5px rgb(0 0 0 / 0.1), 0 8px 10px -6px rgb(0 0 0 / 0.1)',
      '2xl': '0 25px 50px -12px rgb(0 0 0 / 0.25)',
      inner: 'inset 0 2px 4px 0 rgb(0 0 0 / 0.05)',
      none: 'none',
    },
    colors: {
      'white': '#EEEEEE',
      'black': '#000000',
      'black-50': 'rgba(0,0,0,.50)',
      'transparent': 'transparent'
    }
  },
  plugins: [],
}