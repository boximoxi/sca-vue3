export enum DevicesEnum {
    STB = 'stb',
    DESKTOP = 'desktop',
    SMART_TV = 'smart_tv',
    TABLET = 'tablet',
    MOBILE = 'mobile',
    ADD = 'add'
}