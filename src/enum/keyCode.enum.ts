export enum KeyCodeEnum {
    ARROW_LEFT = 'ArrowLeft',
    ARROW_RIGHT = 'ArrowRight',
    ARROW_DOWN = 'ArrowDown',
    ARROW_UP = 'ArrowUp',
    ENTER = 'Enter',
    BACKSPACE = 'BackSpace',
    TAB = 'Tab',
}