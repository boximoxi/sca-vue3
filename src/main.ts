import {createApp} from 'vue'
import App from './App.vue'
import './index.css'
import localization from '@/utils/localization';

createApp(App)
    .use(localization)
    .mount("#app")
