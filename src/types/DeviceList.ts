import type {DevicesModel} from "@/types/Devices.model";

export declare interface DeviceListProps {
    isActive?: boolean,
    addItems?: boolean,
    items: DevicesModel[]
}