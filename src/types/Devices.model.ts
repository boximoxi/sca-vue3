export type DevicesStore = {
    items: DevicesModel[],
    selectedDeviceId?: string,
    updatedItem: boolean,
    isLoading: boolean,
    editedItem: boolean,
    updateList: boolean,
    casId?: string
};

export declare interface DevicesModel {
    id: string,
    name: string,
    label: string,
    deviceType: string,
    createdAt?: string,
}