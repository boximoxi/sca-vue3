import type {DevicesModel} from "@/types/Devices.model";

export declare interface DeviceItemProps {
    data?: DevicesModel,
    label?: string,
    selectedItem?: boolean,
    iconType?: string,
    itemsOnScreen?: boolean
}