export declare interface DefaultButtonsProps {
    label?: string,
    href?: string,
    uppercase?: boolean,
    size?: string,
    margin?: string,
    stretch?: boolean,
    theme?: string,
}