const today = new Date();
const formattedDate = today.toLocaleDateString('hu-HU', { year: 'numeric', month: 'long', day: 'numeric' });
export default [
    {
        id: '1STB',
        name: 'STB',
        label: '1 Box neve',
        deviceType: 'stb',
        createdAt: formattedDate
    }, {
        id: '2',
        name: 'Desktop',
        label: '2 Desktop neve',
        deviceType: 'desktop',
        createdAt: formattedDate
    }, {
        id: '3',
        name: 'Smart TV',
        label: '3 Smart TV neve',
        deviceType: 'smart_tv',
        createdAt: formattedDate
    }, {
        id: '4',
        name: 'Tablet',
        label: '4 Tablet neve',
        deviceType: 'tablet',
        createdAt: formattedDate
    }, {
        id: '5',
        name: 'Mobile',
        label: '5 Mobile neve',
        deviceType: 'mobile',
        createdAt: formattedDate
    }, {
        id: '6',
        name: 'Tablet',
        label: '6 Tablet neve',
        deviceType: 'tablet',
        createdAt: formattedDate
    }, {
        id: '7',
        name: 'Mobile',
        label: '7 Mobile neve',
        deviceType: 'mobile',
        createdAt: formattedDate
    }
]
