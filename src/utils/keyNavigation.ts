class SimpleKeyboardKeyNavigation {
    init = (keyboard: any) => {
        keyboard.registerModule("keyNavigation", (module: any) => {
            module.initMarkerPos = [0, 0];
            module.lastMarkerPos = module.initMarkerPos;

            module.initVars = (layoutName: string) => {
                module.markerPosition = {
                    row: 0,
                    button: 0
                };
                module.layoutName = layoutName || "";
                module.step = keyboard.options.keyNavigationStep || 1;
            };

            module.initMarker = () => {
                const initMarkerPos = module.getButtonAt(...module.lastMarkerPos)
                    ? module.lastMarkerPos
                    : module.initMarkerPos;

                module.setMarker(...initMarkerPos);
            };

            module.getButtonAt = (rowPos: number, btnPos: number) => {
                const layoutName = module.layoutName;
                return keyboard.keyboardDOM.querySelector(
                    `.hg-button[data-skbtnuid="${layoutName}-r${rowPos}b${btnPos}"]`
                );
            };

            module.setMarker = (rowPos: number, btnPos: number) => {
                const buttonDOM = module.getButtonAt(rowPos, btnPos);

                if (buttonDOM) {
                    if (module.markedBtn) {
                        module.markedBtn.classList.remove("hg-keyMarker");
                    }

                    buttonDOM.classList.add("hg-keyMarker");

                    module.markedBtn = buttonDOM;

                    module.lastMarkerPos = [rowPos, btnPos];
                    module.markerPosition = {
                        row: rowPos,
                        button: btnPos
                    };

                    return true;
                } else {
                    if (keyboard.options.debug)
                        console.log(
                            `SimpleKeyboardKeyNavigation: Button default-r${rowPos}b${btnPos} doesnt exist!`
                        );
                    return false;
                }
            };

            module.up = () => {
                const rowPos = module.markerPosition.row - module.step;
                let btnPos;

                if (
                    module.markerPosition.row === 2 &&
                    module.markerPosition.button <= 0
                ) {
                    btnPos = 0;
                } else if (
                    module.markerPosition.row === 2 &&
                    module.markerPosition.button <= 1
                ) {
                    btnPos = 8;
                } else if (
                    module.markerPosition.row === 2 &&
                    module.markerPosition.button <= 2
                ) {
                    btnPos = 10;
                } else if (
                    module.markerPosition.row === 2 &&
                    module.markerPosition.button <= 3
                ) {
                    btnPos = 13;
                } else {
                    btnPos = module.markerPosition.button;
                }

                return module.setMarker(rowPos, btnPos);
            };

            module.down = () => {
                const rowPos = module.markerPosition.row + module.step;
                let btnPos;
                if (
                    module.markerPosition.row === 1 &&
                    module.markerPosition.button <= 7
                ) {
                    btnPos = 0;
                } else if (
                    module.markerPosition.row === 1 &&
                    module.markerPosition.button > 7 &&module.markerPosition.button <= 9
                ) {
                    btnPos = 1;
                } else if (
                    module.markerPosition.row === 1 &&
                    module.markerPosition.button > 9 &&module.markerPosition.button <= 11
                ) {
                    btnPos = 2;
                } else if (
                    module.markerPosition.row === 1 &&
                    module.markerPosition.button > 11
                ) {
                    btnPos = 3;
                } else {
                    btnPos = module.markerPosition.button;
                    if (!module.getButtonAt(rowPos, btnPos)) {
                        for (let i = btnPos; i >= 0; i--) {
                            const checkBtn = module.getButtonAt(rowPos, i);

                            if (checkBtn) {
                                btnPos = i;
                                break;
                            }
                        }
                    }
                }

                return module.setMarker(rowPos, btnPos);
            };

            module.right = () => {
                const rowPos = module.markerPosition.row;
                const btnPos = module.markerPosition.button + module.step;

                return module.setMarker(rowPos, btnPos);
            };

            module.left = () => {
                const rowPos = module.markerPosition.row;
                const btnPos = module.markerPosition.button - module.step;

                return module.setMarker(rowPos, btnPos);
            };

            module.press = () => {
                if (module.markedBtn && module.markedBtn.onclick) {
                    module.markedBtn.onclick();
                }
            };

            module.fn = {};
            module.fn.onRender = keyboard.onRender;

            keyboard.onRender = () => {
                if (
                    keyboard.options.layoutName !== module.layoutName &&
                    keyboard.options.enableKeyNavigation
                ) {
                    if (keyboard.options.debug)
                        console.log(`SimpleKeyboardKeyNavigation: Refreshed`);

                    module.initVars(keyboard.options.layoutName);
                    module.initMarker();
                }

                module.fn.onRender();
            };
        });
    };
}

export default SimpleKeyboardKeyNavigation;