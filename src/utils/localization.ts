import {App, readonly, ref} from 'vue';

interface Localization {
    [key: string]: Record<string, string>;
}

const localization: Localization = {
    en: {
        lang: 'en',
        handlingOfDevices: 'Manage devices',
        devices: 'Devices',
        addDevice: 'Add a new device',
        addDeviceDescription: 'Open the One TV application on the device to be added, then select the Device pairing menu item in Settings, then scan this QR code with your device\'s camera.',
        deviceAdded: 'Device is added',
        buttonBack: 'Back',
        buttonSave: 'Save',
        buttonDeleteDevice: 'Remove device',
        buttonReady: 'Done',
        buttonOk: 'I got it!',
        buttonRemove: 'Remove',
        buttonCancel: 'Cancel',
        editDevice: 'Edit device',
        editDeviceDescription: 'Rename or delete device',
        deviceName: 'Device name',
        deviceNotEdited: 'The device is not editable',
        successfulRemoval: 'Remove was successful',
        successfulEdit: 'Renaming was successful',
        deviceLimit: 'Device limit',
        deviceLimitDescription: 'The maximum device limit is achieved.<by>You cannot add more device, please remove one before add new.',
        removeDevice: 'Do you want to remove the device?',
        genericError: 'Oops… Something went wrong!',
        genericErrorDescription: 'Please try again later.'
    },
    hu: {
        lang: 'hu',
        handlingOfDevices: 'Készülékek kezelése',
        devices: 'Készülékek',
        addDevice: 'Készülék hozzáadása',
        addDeviceDescription: 'Nyisd meg a hozzáadandó készülékeden a One TV alkalmazást, majd a Beállításokon belül válaszd ki a Párosítás menüpontot, majd készüléked kamerájával olvasd be ezt a QR kódot.',
        deviceAdded: 'Készülék hozzáadva',
        buttonBack: 'Vissza',
        buttonSave: 'Mentés',
        buttonDeleteDevice: 'Készülék törlése',
        buttonReady: 'Kész',
        buttonOk: 'Rendben',
        buttonRemove: 'Eltávolítás',
        buttonCancel: 'Mégsem',
        editDevice: 'Készülék szerkesztése',
        editDeviceDescription: 'Átnevezés vagy törlés',
        deviceName: 'Készülék neve',
        deviceNotEdited: 'A készülék nem szerkeszthető!',
        successfulRemoval: 'Sikeresen eltávolítottad a készüléket.',
        successfulEdit: 'Sikeresen átnevezted a készüléket.',
        deviceLimit: 'Készülék limit',
        deviceLimitDescription: 'Elérted a hozzáadható készülékek maximális számát.<br>Új hozzáadása előtt el kell távolítanod egy készüléket.',
        removeDevice: 'Biztos, hogy eltávolítod ezt a készüléket?',
        genericError: 'Hiba történt',
        genericErrorDescription: 'Próbáld újra később.'
    },
};

const defaultLanguage = 'hu';
const selectedLanguage = ref<string>(defaultLanguage);

export const setLanguage = (lang: string): void => {
    if (localization[lang]) {
        selectedLanguage.value = lang;
    }
};

export const translate = (key: string): string => {
    const lang = selectedLanguage.value;
    if (localization[lang] && localization[lang][key]) {
        return localization[lang][key];
    }
    return localization[defaultLanguage][key] || key;
};

export default {
    install: (app: App) => {
        app.provide('selectedLanguage', readonly(selectedLanguage));
        app.config.globalProperties.$t = translate
    },
};

declare module '@vue/runtime-core' {
    interface ComponentCustomProperties {
        $t: (key: string) => string
    }
}

export {}