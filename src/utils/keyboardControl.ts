import {ref, onMounted, onBeforeUnmount, Ref} from 'vue';

interface TimeStamp {
    value: Date;
}

interface KeyHandler {
    lastPressedKey: Ref<KeyType>;
    timeStamp: Ref<TimeStamp>;
}

type KeyType = string | undefined;

export const useKeyHandler = (): KeyHandler  => {
    const lastPressedKey = ref<KeyType>( undefined);
    const timeStamp = ref<TimeStamp>({ value: new Date() });

    const handleKeyDown = (event: KeyboardEvent) => {
        lastPressedKey.value = event.key ? event.key : event.code;
        timeStamp.value = { value: new Date() };
    };

    onMounted(() => {
        window.addEventListener('keydown', handleKeyDown);
    });

    onBeforeUnmount(() => {
        window.removeEventListener('keydown', handleKeyDown);
    });

    return {
        lastPressedKey,
        timeStamp
    };
}