import {ref} from 'vue'

interface TimeStamp {
    value: number;
}

interface Notification {
    title: string;
    show: boolean;
    timeStamp: number | TimeStamp;
}

const state = ref<Notification>({
    title: '',
    show: false,
    timeStamp: { value: new Date().getTime() } as TimeStamp,
});

const mutations = {
    showNotification (title: string) : void {
        state.value.title = title;
        state.value.show = true;
        state.value.timeStamp = new Date().getTime();
    }
};

export const useNotification = () => {
    return {
        state,
        mutations
    }
}