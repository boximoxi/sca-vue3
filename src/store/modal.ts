import {markRaw, ref} from 'vue';

export type Modal = {
    isOpen: boolean,
    view: object,
    removedItem: boolean
};

const state = ref<Modal>({
    isOpen: false,
    view: {},
    removedItem: false
});

const mutations = {
    open(view: object): void {
        state.value.isOpen = true;
        state.value.view = markRaw(view);
    },
    close(removedItem?: boolean): void {
        state.value.isOpen = false;
        state.value.view = {};
        state.value.removedItem = removedItem || false;
    }
}

export const useModal = () => {
    return {
        state,
        mutations
    }
}