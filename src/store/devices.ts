import {DevicesModel, DevicesStore} from '@/types/Devices.model';
import deviceItems from '@/assets/data/deviceItems';
import {ref} from 'vue';
import {DevicesEnum} from '@/enum/devices.enum';
import axios from 'axios';

const state = ref<DevicesStore>({
    items: [],
    selectedDeviceId: undefined,
    updatedItem: false,
    isLoading: false,
    editedItem: false,
    updateList: true,
    casId: undefined
});

const mutations = {
    addItem (item: DevicesModel): void {
        if (!item) return;
        state.value.items.push(item);
    },
    fetchDevices: (casId: DevicesStore) : void => {
        state.value.isLoading = true;

        setTimeout(() => {
            if (!state.value.items.length) {
                state.value.items = deviceItems.filter((item: DevicesModel) => item.deviceType === DevicesEnum.STB || item.deviceType === DevicesEnum.SMART_TV);
            }

            state.value.isLoading = false;
        }, 1000);
    },
    findIndexById (id: string): number {
        return state.value.items.findIndex((item: DevicesModel) => item.id === id);
    },
    deleteItem (id: string): void {
        const index: number = this.findIndexById(id);
        if (index === -1) return;
        state.value.items.splice(index, 1);
    },
    updateItem (id: string, label: string) : void {
        if (!id || !label) return;
        const index: number = this.findIndexById(id);
        if (index !== -1) {
            state.value.items[index].label = label;
            state.value.updatedItem = true;
        }
    },
};

const getters = {
    getMutableList : () => {
        return state.value.items.filter((item: DevicesModel) => item.deviceType !== DevicesEnum.STB && item.deviceType !== DevicesEnum.SMART_TV);
    },
    getNotMutableList: () => {
        return state.value.items.filter((item: DevicesModel) => item.deviceType === DevicesEnum.STB || item.deviceType === DevicesEnum.SMART_TV);
    },
    getCurrentItem: () => {
        return state.value.items.find((item: DevicesModel) => item.id === state.value.selectedDeviceId);
    },
};

export const useDevices = () => {
    return {
        state,
        mutations,
        getters,
    };
};